//logs.js
const util = require('../../utils/util.js')

Page({
  data: {
    logs: [],
    name:null,
    isScroll:true
  },
  onLoad: function (option) {
    console.log(option)
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        return util.formatTime(new Date(log))
      }),
      name:option.name
    })
  }
})
