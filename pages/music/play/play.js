// pages/music/play/play.js
const innerAudioContext = wx.createInnerAudioContext();
const util = require("../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = options.id;
    // const id = "003OUlho2HcRHC";
    const img = options.img;
    const lrc = options.lrc;
    const key = "6BFDD0DFE8A88C65E5D7942967AE84A1F7BC2A96A9120C15A5032483EA5D0659";
    util.utilGet("https://c.y.qq.com/base/fcgi-bin/fcg_music_express_mobile3.fcg?g_tk=5381&jsonpCallback=MusicJsonCallback8307607419317575&loginUin=0&hostUin=0&format=json&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq&needNewCode=0&cid=205361747&callback=MusicJsonCallback8307607419317575&uin=0&songmid="+id+"&filename=C400"+id+".m4a&guid=890047478",(res)=>{
    
    const result = JSON.stringify(res);
    const key = result.substring(result.indexOf("vkey")+9,result.length-8) ;
    innerAudioContext.autoplay = true
    innerAudioContext.src = 'http://dl.stream.qqmusic.qq.com/C400'+id+'.m4a?vkey=' + key+'&guid=890047478&uin=0&fromtag=66';
    innerAudioContext.onPlay(() => {
      console.log('开始播放')
    })
    innerAudioContext.onError((res) => {
      console.log(res.errMsg)
      console.log(res.errCode)
    })
    },()=>{

    })
    

    
  },

  // MusicJsonCallback8307607419317575(cid,code,data,userip){
  //   console.log(data);
  // },

  /** 
   * 初始化播放的方法
   * */
   
  initPlay(){

  },

  audioPlay(){
    innerAudioContext.play();
  },
  audioPause(){
    innerAudioContext.pause();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
   
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.audioPause();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})