// pages/music/music-list.js
const util = require("../../utils/util.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    musicList:[],
    searchName:"最新",
    pageNum:1,
    pageSize:10
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const name = this.data.searchName;
    const pageNum = this.data.pageNum;
    const pageSize = this.data.pageSize;
    this.queryData(name,pageSize,pageNum);
  },

  // 进入音乐播放
  gotoPlay(item){
    const i = item.currentTarget.dataset.item;

    const id = i.strMediaMid;
    const img = 1;
    const lrc = 1;
    wx.navigateTo({url:"./play/play?id="+id+"&img="+img+"&lrc="+lrc});
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  /**处理出入框 */
  inputChange(e){
    var inputValue = e.detail.value;
    this.setData({
      searchName:inputValue
    })
    this.queryData(this.data.searchName,this.data.pageSize,this.data.pageNum);
  },
  /**
   * 查询数据
   */
  queryData(name,pageSize,pageNum){
    var url = "https://c.y.qq.com/soso/fcgi-bin/client_search_cp?t=0& n=" + pageSize + "&aggr=1&cr=1&loginUin=0&format=json& inCharset=GB2312&outCharset=utf-8&notice=0& platform=jqminiframe.json&needNewCode=0&p=" + pageNum + "&catZhida=0& remoteplace=sizer.newclient.next_song&w=" + name;
    util.utilGet(url, (data) => {
      console.log(data);
      const list = data.data.song.list;
      this.setData({
        musicList: list
      })
    }, (data) => {
      console.log("失败" + data.message);
    })
  }

})