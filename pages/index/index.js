
//index.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js')

Page({
  data: {
    motto: '开启小程序之旅',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function() {
    // var name = this.data.userInfo.nickName;
    // wx.navigateTo({
    //   url: '../test/test?name='+name
    // })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  onHide:function(){
    console.log("隐藏触发")
  },
  onShow:function(){
    console.log("显示触发")
  },
  /**获取用户信息 */
  getUserInfo: function(e) {
    console.log(app)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  /**进入首页 */
  gotoHome(){
    wx.navigateTo({
      url: '../songsheet/song_sheet'
    })
    
  },
  /**进入搜索列表 */
   gotoMusicList() {
    wx.navigateTo({
      url: '../music/music-list'
    })

  }
})
