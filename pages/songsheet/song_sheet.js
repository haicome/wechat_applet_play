// pages/songsheet/song_sheet.js
const api = require("../../utils/util.js");
const backgroundAudioManager = wx.getBackgroundAudioManager();
var time = 0;
var interval = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showMusicHome: true,
    musicHomeFontSize: 40,
    mineFontSize: 35,
    songSheetList: [],
    isPlay: false,
    currentSong: null,
    currentSongIndex: 0,
    lastX: 0,          //滑动开始x轴位置
    lastY: 0,          //滑动开始y轴位置
    text: "没有滑动",
    currentGesture: 0, //标识手势
    maskHidden:true,
    scrollY:true,
    height: 1040
  },
  baseHost: null,
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this;
    const url = "/haicome/getQMusicSongSheet";
    api.utilGet(url, null, (res) => {
      const list = res.data.data.list;
      console.log(2)
      that.setData({
        songSheetList: list
      })
    }, res => {
      console.log(res);
    });

    backgroundAudioManager.onEnded(function () {
      const index = that.data.currentSongIndex++;
      that.getCurrent(that.data.songSheetList[index], index);
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /**以下---自定义方法 */

  /**切换选项卡 */
  switchTab: function (e) {
    console.log(e);
    const targetName = e.currentTarget.dataset.name;
    switch (targetName) {
      case 'musicHome':
        this.showMusicHome();
        break;
      case 'mine':
        this.showMine();
        break;
    }
  },

  /**显示音乐馆 */
  showMusicHome: function () {
    if (this.data.showMusicHome) {
      return;
    } else {
      this.setData({
        showMusicHome: true,
        musicHomeFontSize: 40,
        mineFontSize: 35

      })
    }
  },
  /**显示我的 */
  showMine: function () {
    if (!this.data.showMusicHome) {
      return;
    } else {
      this.setData({
        showMusicHome: false,
        musicHomeFontSize: 35,
        mineFontSize: 40
      })
    }
  },
  playOrStop: function (e) {
    if (this.data.isPlay) {
      backgroundAudioManager.pause();

      this.setData({
        isPlay: false
      })
    } else {
      backgroundAudioManager.play();
      this.setData({
        isPlay: true
      })
    }
  },
  /**播放歌单 */
  palySongSheet: function (e) {
    const dissid = e.currentTarget.dataset.id;
    const url = "/haicome/getQMusicSongSheetDetail";
    const that = this;
    api.utilGet(url, { dissid: dissid }, (res) => {
      const songlist = res.data.cdlist[0].songlist;
      console.log(songlist);
      that.setData({
        songList: songlist,  //歌单中的歌曲
        height:"980rpx"
      })
      //自动播放歌单中的第一首歌曲
      that.getCurrent(songlist[0], 0);
    }, res => {
      console.log(res);

    })
  },
  //获取传入的歌曲并播放
  getCurrent: function (song, index) {
    const that = this;
    const albummid = song.albummid;
    const id = song.strMediaMid;
    that.setData({
      currentSong: song,
      currentSongIndex: index
    })
    api.utilGet("/haicome/getQMusicSongByAlbummid", { albummid: albummid }, (res) => {
      that.playSong(id);
      console.log(res);
      if (!res.data.data){
        if (that.data.currentSongIndex < that.data.songSheetList.length - 1) {
          const index = ++that.data.currentSongIndex;
          this.getCurrent(that.data.songList[index], index);
        } else {
          const index = that.data.currentSongIndex;
          that.getCurrent(that.data.songList[index], index);
        }
        return;
      }
      const mid = res.data.data.mid;
      const url = "https://y.gtimg.cn/music/photo_new/T002R300x300M000" + mid + ".jpg?max_age=2592000";
      that.setData({
        currentSongImgageUrl: url
      })
    }, () => {
      console.log("获取歌曲详情失败！");
    })
  },
  //播放指定的歌曲vkey
  playSong: function (id) {
    const that = this;
    api.utilGet("/haicome/getQMusicSongVkeyById", { id: id }, (res) => {
      if (res.data.length!=112){
        if (that.data.currentSongIndex < that.data.songSheetList.length - 1) {
          const index = ++that.data.currentSongIndex;
          this.getCurrent(that.data.songList[index], index);
        } else {
          const index = that.data.currentSongIndex;
          that.getCurrent(that.data.songList[index], index);
        }
      return;
      }
      backgroundAudioManager.src = 'http://dl.stream.qqmusic.qq.com/C400' + id + '.m4a?vkey=' + res.data + '&guid=890047478&uin=0&fromtag=66';
      backgroundAudioManager.play();
      this.setData({
        isPlay: true
      })
    }, () => {
      console.log("获取歌曲详情失败！");
    })
  },
  ////////////////////////////

  //滑动开始事件
  handletouchtart: function (event) {
    this.data.lastX = event.touches[0].pageX
    this.data.lastY = event.touches[0].pageY
    // 使用js计时器记录时间    
    interval = setInterval(function () {
      time++;
    }, 100);
  },
  //滑动结束事件
  handletouchend: function (event) {
    var currentX = event.changedTouches[0].pageX
    var currentY = event.changedTouches[0].pageY
    var tx = currentX - this.data.lastX
    var ty = currentY - this.data.lastY
    var text = ""
    //左右方向滑动
    if (Math.abs(tx) > Math.abs(ty)) {
      if (tx < -40 && time <10) {

        //"向左滑动"
        if (this.data.currentSongIndex < this.data.songSheetList.length - 1) {
          const index = ++this.data.currentSongIndex;
          this.getCurrent(this.data.songList[index], index);
        } else {
          const index = this.data.currentSongIndex;
          this.getCurrent(this.data.songList[index], index);
        }
      }
      else if (tx > 40 && time < 10) {
       // "向右滑动"
        
        if (this.data.currentSongIndex > 0) {
          const index = --this.data.currentSongIndex;
          this.getCurrent(this.data.songList[index], index);
        } else {
          this.getCurrent(this.data.songList[0], 0);
        }
      }

    }
    // //上下方向滑动
    // else {
    //   if (ty < 0)
    //     text = "向上滑动"
    //   else if (ty > 0)
    //     text = "向下滑动"
    // }
    clearInterval(interval); // 清除setInterval
    time = 0;
  },

  //** */
  _closeMaskEvent:function(){
    this.setData({
      maskHidden:true,
      scrollY:true
    })
  },
  /**显示播放列表 */
  showSongList:function(){
    this.setData({
      maskHidden: false,
      scrollY:false
    })
  },

  playThisSong:function(e){
    const index = e.currentTarget.dataset.index;
    this.getCurrent(this.data.songList[index], index);
  },

  //**搜索 */
  search:function(e){
    const that = this;
    const search = e.detail.value;
    api.utilGet("/haicome/getQMusicSongSearchList", { search: search},function(res){
      const list = res.data.data.list;
      that.setData({
        songSheetList: list
      })
    })
  }
})