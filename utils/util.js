const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


//post请求封装
const post = function (url, data, success, fail, complete){
  const baseHost = getApp().globalData.baseHost;
  url = baseHost + url;
  wx.request({
    url: url,
    method:'POST',
    data:data,
    success:(res)=>{
     
        success(res);
      
    },
    fail:res=>{
      console.log(res);
      fail(res.errMsg);
    },
    complete: complete
  })
}

//get请求封装
const utilGet = function (url, data,success, fail, complete) {
  const baseHost = getApp().globalData.baseHost;
  url = baseHost + url;
  wx.request({
    url: url,
    method: 'GET',
    data: data,
    success: (res) => {
      
        success(res);
 
    },
    fail: res => {
      console.log(res);
      fail(res.errMsg);
    },
    complete: complete
  })
}

module.exports = {
  formatTime: formatTime,
  post:post,
  utilGet: utilGet
}
